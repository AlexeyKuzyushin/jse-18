package ru.rencredit.jschool.kuzyushin.tm.command.task;

import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskViewByIdCommand extends AbstractTaskShowCommand {

    @Override
    public String name() {
        return "task-view-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAILED]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }
}
