package ru.rencredit.jschool.kuzyushin.tm.command.system;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display terminal commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final AbstractCommand command: commands) {
            StringBuilder result = new StringBuilder();
            if (command.name() != null && !command.name().isEmpty()) result.append(command.name());
            if (command.arg() != null && !command.arg().isEmpty()) result.append(", ").append(command.arg());
            if (command.description() != null && !command.description().isEmpty())
                result.append(": ").append(command.description());
            System.out.println(result.toString());
        }
        System.out.println("[OK]");
    }
}
