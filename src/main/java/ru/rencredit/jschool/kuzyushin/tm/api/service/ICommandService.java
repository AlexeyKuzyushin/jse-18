package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommandList();
}
