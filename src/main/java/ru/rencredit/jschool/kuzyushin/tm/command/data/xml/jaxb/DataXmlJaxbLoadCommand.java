package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.jaxb;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlJaxbLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-jaxb-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        final File file = new File(DataConstant.FILE_XML_JAXB);
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        final Domain domain = (Domain) unmarshaller.unmarshal(file);
        serviceLocator.getDomainService().load(domain);

        System.out.println("[OK]");
    }
}