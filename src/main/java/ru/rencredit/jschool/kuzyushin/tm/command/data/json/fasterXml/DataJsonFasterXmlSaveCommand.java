package ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataJsonFasterXmlSaveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-json-fasterxml-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save data to json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");

        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
