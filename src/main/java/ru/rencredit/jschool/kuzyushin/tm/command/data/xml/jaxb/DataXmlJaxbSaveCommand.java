package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.jaxb;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public class DataXmlJaxbSaveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-jaxb-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConstant.FILE_XML_JAXB);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
