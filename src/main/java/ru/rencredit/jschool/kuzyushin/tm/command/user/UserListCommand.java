package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.util.List;

public final class UserListCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show user list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST USERS]");
        final List<User> users = serviceLocator.getUserService().findAll();
        int index = 1;
        for (User user: users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getLogin());
            index++;
        }
        System.out.println("[OK]");
    }
}
