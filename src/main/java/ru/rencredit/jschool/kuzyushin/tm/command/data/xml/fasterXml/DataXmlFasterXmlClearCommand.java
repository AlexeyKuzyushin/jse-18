package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.fasterXml;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;

import java.io.File;
import java.nio.file.Files;

public class DataXmlFasterXmlClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-fasterxml-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE XML FILE]");
        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }
}
