package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.fasterXml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

import java.io.FileInputStream;

public class DataXmlFasterXmlLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-fasterxml-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_XML);
        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
        serviceLocator.getDomainService().load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }
}