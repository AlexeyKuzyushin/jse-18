package ru.rencredit.jschool.kuzyushin.tm.command.project;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeOneById(userId, id);
        if (project == null) {
            System.out.println("[FAILED]");
        } else {
            System.out.println("[OK]");
        }
    }
}
