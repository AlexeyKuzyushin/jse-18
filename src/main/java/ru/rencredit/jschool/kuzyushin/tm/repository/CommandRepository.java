package ru.rencredit.jschool.kuzyushin.tm.repository;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.authentication.LoginCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.authentication.LogoutCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.base64.DataBase64ClearCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.base64.DataBase64LoadCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.base64.DataBase64SaveCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.binary.DataBinaryClearCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.binary.DataBinaryLoadCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.binary.DataBinarySaveCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml.DataJsonFasterXmlClearCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml.DataJsonFasterXmlLoadCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml.DataJsonFasterXmlSaveCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.json.jaxb.DataJsonJaxbClearCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.json.jaxb.DataJsonJaxbLoadCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.json.jaxb.DataJsonJaxbSaveCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.xml.fasterXml.DataXmlFasterXmlClearCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.xml.fasterXml.DataXmlFasterXmlLoadCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.xml.fasterXml.DataXmlFasterXmlSaveCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.xml.jaxb.DataXmlJaxbClearCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.xml.jaxb.DataXmlJaxbLoadCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.data.xml.jaxb.DataXmlJaxbSaveCommand;
import ru.rencredit.jschool.kuzyushin.tm.command.project.*;
import ru.rencredit.jschool.kuzyushin.tm.command.system.*;
import ru.rencredit.jschool.kuzyushin.tm.command.task.*;
import ru.rencredit.jschool.kuzyushin.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[] {
        LoginCommand.class, LogoutCommand.class,

        UserListCommand.class, UserRegistryCommand.class, UserRemoveByIdCommand.class,
        UserUpdateEmailCommand.class, UserUpdateFirstNameCommand.class, UserUpdateLastNameCommand.class,
        UserUpdateLoginCommand.class, UserUpdateMiddleNameCommand.class, UserUpdatePasswordCommand.class,
        UserViewProfileCommand.class, UserLockCommand.class, UserUnlockCommand.class,
        UserRemoveByLoginCommand.class,

        ProjectClearCommand.class, ProjectCreateCommand.class, ProjectListCommand.class,
        ProjectRemoveByIdCommand.class, ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class,
        ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class, ProjectViewByIdCommand.class,
        ProjectViewByIndexCommand.class, ProjectViewByNameCommand.class,

        TaskClearCommand.class, TaskCreateCommand.class, TaskListCommand.class, TaskRemoveByIdCommand.class,
        TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class, TaskUpdateByIdCommand.class,
        TaskUpdateByIndexCommand.class, TaskViewByIdCommand.class, TaskViewByIndexCommand.class,
        TaskViewByNameCommand.class,

        AboutCommand.class, ExitCommand.class, HelpCommand.class, ShowArgCommand.class, ShowCmdCommand.class,
        SystemInfoCommand.class, VersionCommand.class,

        DataBinaryLoadCommand.class, DataBinarySaveCommand.class, DataBinaryClearCommand.class,
        DataBase64LoadCommand.class, DataBase64SaveCommand.class, DataBase64ClearCommand.class,
        DataXmlFasterXmlLoadCommand.class, DataXmlFasterXmlSaveCommand.class, DataXmlFasterXmlClearCommand.class,
        DataJsonFasterXmlLoadCommand.class, DataJsonFasterXmlSaveCommand.class, DataJsonFasterXmlClearCommand.class,
        DataXmlJaxbLoadCommand.class, DataXmlJaxbSaveCommand.class, DataXmlJaxbClearCommand.class,
        DataJsonJaxbLoadCommand.class, DataJsonJaxbSaveCommand.class, DataJsonJaxbClearCommand.class
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz: COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }
}
