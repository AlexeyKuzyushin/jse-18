package ru.rencredit.jschool.kuzyushin.tm.command.task;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASKS]");
        System.out.println("[ENTER NAME]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }
}
