package ru.rencredit.jschool.kuzyushin.tm.command.data.xml.fasterXml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataXmlFasterXmlSaveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-xml-fasterxml-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");

        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ObjectMapper objectMapper = new XmlMapper();
        final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
