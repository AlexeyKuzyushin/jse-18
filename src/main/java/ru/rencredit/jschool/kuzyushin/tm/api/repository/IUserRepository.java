package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    void add(List<User> users);

    void add(User... users);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    void load (List<User> users);

    void load (User... users);

    void clear();
}
