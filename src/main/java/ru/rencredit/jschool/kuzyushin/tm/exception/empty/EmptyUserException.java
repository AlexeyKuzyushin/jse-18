package ru.rencredit.jschool.kuzyushin.tm.exception.empty;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("Error! User does not exist...");
    }
}
