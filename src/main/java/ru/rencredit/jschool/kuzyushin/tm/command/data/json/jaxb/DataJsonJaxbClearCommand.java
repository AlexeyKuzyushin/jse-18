package ru.rencredit.jschool.kuzyushin.tm.command.data.json.jaxb;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;

import java.io.File;
import java.nio.file.Files;

public class DataJsonJaxbClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-json-jaxb-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE JSON FILE]");
        final File file = new File(DataConstant.FILE_JSON_JAXB);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }
}
