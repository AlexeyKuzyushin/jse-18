package ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataJsonFasterXmlLoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-json-fasterxml-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_JSON);
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
        serviceLocator.getDomainService().load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }
}
